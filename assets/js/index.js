
function addToCart() {
    // Get product details
    alert("Product added to cart!");
    var productName = document.getElementById("ProductName").innerText;
    var productPrice = parseFloat(document.getElementById("price").innerText);
    

    // Create or update a shopping cart item
    var cartItem = {
        name: productName,
        price: productPrice,
        quantity: 1
    };

    // Convert the cart item to JSON and store it in localStorage
    var existingCart = JSON.parse(localStorage.getItem("cart")) || [];
    existingCart.push(cartItem);
    localStorage.setItem("cart", JSON.stringify(existingCart));

    
}

document.addEventListener("DOMContentLoaded", function () {
    // Load and display cart items from localStorage
    var cartItems = JSON.parse(localStorage.getItem("cart")) || [];
    displayCartItems(cartItems);
});

function displayCartItems(items) {
    var cartItemsDiv = document.getElementById("Cart-items");

    // Clear previous content
    cartItemsDiv.innerHTML = "";

    // Display each item in the cart
    items.forEach(function (item) {
        var itemDiv = document.createElement("div");
        itemDiv.innerHTML = `<p>${item.name} - $${item.price.toFixed(2)} (${item.quantity} items)</p>`;
        cartItemsDiv.appendChild(itemDiv);
    });
}